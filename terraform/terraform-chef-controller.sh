#!/bin/bash

# generally, use /tmp as working directory
cd /tmp

# add Fedora epel repository
yum -y install epel-release
#rpm -Uvh http://dl.fedoraproject.org/pub/epel/7/x86_64/e/epel-release-7-5.noarch.rpm

# patch CentOS to most frequent level
yum -y update

# prepare to build a current version of ansible (>= 2.0) to support OpenStack
# psycopg2 for Postgres installation
yum --enablerepo=epel -y install make rpm-build python2-devel python-psycopg2 python-pip asciidoc git expect libffi-devel openssl-devel

# install OpenStack shade client lib to control the OpenStack by API
pip install --upgrade pip
pip install shade "python-openstackclient==2.3.1"

# install terraform
curl https://releases.hashicorp.com/terraform/0.11.7/terraform_0.11.7_linux_amd64.zip > terraform.zip
unzip terraform.zip -d /usr/local/bin


