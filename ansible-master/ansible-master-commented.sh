#!/bin/bash

# please install openstack-client first


# add Fedora epel repository
sudo yum -y install epel-release
#rpm -Uvh http://dl.fedoraproject.org/pub/epel/7/x86_64/e/epel-release-7-5.noarch.rpm

# patch CentOS to most frequent level
sudo yum -y update --skip-broken

# prepare to build a current version of ansible (>= 2.0) to support OpenStack
# psycopg2 for Postgres installation
sudo yum --enablerepo=epel -y install make rpm-build python36-psycopg2 python36-docutils asciidoc git expect libffi-devel openssl-devel --skip-broken

# install OpenStack+OTC extension+shade client lib
# to control the OpenStack by API
sudo /usr/bin/pip3 install --upgrade pip setuptools packaging
sudo /usr/local/bin/pip3 install shade # for openstack vi ansible

# checkout current version of ansible from git
# Ansible 2.x is not available as ready-made rpm package yet in repos
# generally, use /tmp as working directory
#cd /tmp
#git clone git://github.com/ansible/ansible.git --recursive
#cd /tmp/ansible
#sudo /usr/local/bin/pip3 install -r requirements.txt
#make rpm
#sudo yum install -y /tmp/ansible/rpm-build/ansible-*.noarch.rpm
#rm -rf /tmp/ansible

# alternatively, install ansible from repo
# see also: http://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html
sudo yum install https://releases.ansible.com/ansible/rpm/release/epel-7-x86_64/ansible-2.7.9-1.el7.ans.noarch.rpm

# install a proxy on the ansible server a internet access point for local VMs
# for cases where a NATTING gateway is not an option
#sudo yum install -y squid
#sudo firewall-cmd --permanent --add-port=3128/tcp
#
#sudo systemctl enable squid
#sudo systemctl start squid
