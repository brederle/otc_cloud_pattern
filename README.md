# README #

### What is this repository for? ###

* Quick summary
This repository demonstrates how to auto-deploy infrastructure on the brand-new Open Telekom Cloud (OTC) with Ansible. It contains exemplary but fully executable playbooks that create different, complex cloud applications on your OTC tenant. It is a perfect example to introduce OTC and immutable infrastructures with your company. Get cloud-native with our help!

* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
~~~~
git clone https://bitbucket.org/brederle/otc_cloud_pattern 
~~~~
* Configuration
* Dependencies
~~~~
git pull origin master
~~~~
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact