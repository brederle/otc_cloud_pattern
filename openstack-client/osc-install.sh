#!/bin/bash

# add Fedora epel repository
sudo yum -y install epel-release

# patch CentOS to most frequent level
sudo yum -y update --skip-broken

# install python3 (to /usr/local by default in CentOS!)
sudo yum --enablerepo=epel -y install python36-devel python36-pip git --skip-broken
sudo /usr/bin/pip3 install --upgrade pip setuptools packaging

# Install openstackclient (newest, known working version)
# looks like CentS changes install directory for pip3
# after upgrade
sudo /usr/local/bin/pip3 install "python-openstackclient==3.18.0"

# copy certificates
cp -u otc_certs.pem $HOME

mkdir -p --mode=700 $HOME/.config/openstack
cp -u clouds.yaml $HOME/.config/openstack/clouds.yaml

# install newest OTC openstack extensions
cd /tmp
git clone https://github.com/OpenTelekomCloud/python-otcextensions
cd python-otcextensions
sudo /usr/local/bin/pip3 install -r requirements.txt
sudo python3 setup.py install
cd /tmp 
sudo rm -rf /tmp/python-otcextensions

echo
echo -----------------------------------------------------
echo Please edit ~/.config/openstack/clouds.yaml for your 
echo tenant, project and API user.
echo Use client in the following way:
echo $ openstack --os-cloud otc ...
echo -----------------------------------------------------


