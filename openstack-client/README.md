# openstack commandline client for Open Telekom CLoud (OTC)

This directory contains an install script and auxiliary files to gent an openstack commandline client up and running, enhanced by OTC python extensions.

* osc-install.sh: the installation script (for CentOS/RehHat/EulerOS). It also documents the required installation steps. 
* otc_certs.pem: Open Telekom Cloud certificates to enable trusted connections from client. Copied to user home directory.
* cloud.yaml: example config for OTC API access. Copied to ~/.config/cloud.yaml. PLease modify with your crendetials.  

See also:
https://github.com/OpenTelekomCloud/python-otcextensions

